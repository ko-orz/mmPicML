var getPixels = require("get-pixels");
var imgPath = '../data/4.png';
const D_941 = 3;
// var imgPath = './test2.png'

var PNG = require('./png.js');
getPixels(imgPath, function (err, pixels) {
    if (err) {
        console.log(err)
        console.log("Bad image path", imgPath);
        callback("图片格式错");
        return
    }
    console.log(imgPath);
    // console.log(pixels)
    console.log('尺寸', pixels.shape)
    const WIDTH = pixels.shape[0];
    const HEIGHT = pixels.shape[1];
    const COLORS = pixels.shape[2];
    let colorData = pixels.data;
    let pixArr = [];
    let _i = 0;
    let p = new PNG(WIDTH, HEIGHT);

    for (var y = 0; y < HEIGHT; y++) {
        for (var x = 0; x < WIDTH; x++) {
            if (!pixArr[x]) pixArr[x] = [];
            pixArr[x][y] = [colorData[_i], colorData[_i + 1], colorData[_i + 2], colorData[_i + 3]];
            // p.setPixel(x, y, colorData[_i]*256*256*256 + colorData[_i+1]*256*256 + colorData[_i+2]*256 + colorData[_i+3])
            _i += 4;
        }
    }
    let newPic = mohu(mohu(mohu(pixArr)));
    let newPic2 = mohu(mohu(mohu(newPic)))
    // newPic = mohu(mohu(mohu(newPic2)))
    // newPic2 = mohu(mohu(mohu(newPic)))
    newPic3 = mohu(mohu(mohu(newPic2)))
    // let newPic3 = mohu(pixArr);
    for (var y = 0; y < HEIGHT; y++) {
        for (var x = 0; x < WIDTH; x++) {
            p.setPixel(x, y, getRandomColor(newPic3[x][y][0]) * 256 * 256 * 256 + getRandomColor(newPic3[x][y][1]) * 256 * 256 + getRandomColor(newPic3[x][y][2]) * 256 + getRandomColor(newPic3[x][y][3]))
        }
    }
    // for (var y = 0; y < HEIGHT; y++) {
    //     for (var x = 0; x < WIDTH; x++) {
    //         p.setPixel(x, y,get941(x, y, 0, pixArr)*256*256*256 + get941(x, y, 1, pixArr)*256*256 + get941(x, y, 2, pixArr)*256 + get941(x, y, 3, pixArr));
    //         _i+=4;
    //     }
    // }
    function getRandomColor(src, d=0) {
        src += Math.random()*d|0-d/2|0;
        if (src > 255) src = 255;
        if (src < 0) src = 0;
        return src;
    }
    var aaaa = 20;
    function mohu(c) {
        let newPic = [];
        aaaa-= 5;
        for (var y = 0; y < HEIGHT; y++) {
            for (var x = 0; x < WIDTH; x++) {
                if (!newPic[x]) newPic[x] = [];
                newPic[x][y] = getMixPixel(x, y, c);
            }
        }
        return newPic
    }
    function getMixPixel(x, y, tar) {
        return [
            get941(x, y, 0, tar),
            get941(x, y, 1, tar),
            get941(x, y, 2, tar),
            get941(x, y, 3, tar)
        ]
    }
    // 9合1
    function get941(x, y, i, tar, d) {
        if (d==undefined) {d=D_941}
        // d=aaaa>0?aaaa:1;
        let lx = (x - d) > 0 ? (x - d) : 0;
        let rx = (x + d) >= (WIDTH - d) ? (WIDTH - d) : (x + d);
        let uy = (y - d) > 0 ? (y - d) : 0;
        let dy = (y + d) >= (HEIGHT - d) ? (HEIGHT - d) : (y + d);
        let lx2 = (x - 2*d) > 0 ? (x - 2*d) : 0;
        let rx2 = (x + 2*d) >= (WIDTH - 2*d) ? (WIDTH - 2*d) : (x + 2*d);
        let uy2 = (y - 2*d) > 0 ? (y - 2*d) : 0;
        let dy2 = (y + 2*d) >= (HEIGHT - 2*d) ? (HEIGHT - 2*d) : (y + 2*d);
        // 模糊
        // return (tar[lx][uy][i] + tar[lx][y][i] + tar[lx][dy][i] + tar[x][uy][i] + tar[x][y][i] + tar[x][dy][i] + tar[rx][uy][i] + tar[rx][y][i] + tar[rx][dy][i]) / 9 | 0
        // 中点为重 虚化模糊
        // return (tar[lx][uy][i] + tar[lx][y][i] + tar[lx][dy][i] + tar[x][uy][i] + 10*tar[x][y][i] + tar[x][dy][i] + tar[rx][uy][i] + tar[rx][y][i] + tar[rx][dy][i]) / 18 | 0
        // 突出
        // let final = (-tar[lx][uy][i] - tar[lx][y][i] - tar[lx][dy][i] - tar[x][uy][i] + 17*tar[x][y][i] - tar[x][dy][i] - tar[rx][uy][i] - tar[rx][y][i] - tar[rx][dy][i]) / 9 | 0
        // if (final > 255) final = 255;
        // if (final < 0) final = 0;
        // return final;
        // 突出2
        let final = (
            - tar[lx2][uy2][i]  - tar[lx][uy2][i]   - tar[x][uy2][i]    - tar[rx][uy2][i]   - tar[rx2][uy2][i]
            - tar[lx2][uy][i]   - tar[lx][uy][i]    - tar[x][uy][i]     - tar[rx][uy][i]    - tar[rx2][uy][i]
            - tar[lx2][y][i]    - tar[lx][y][i]     + 124 * tar[x][y][i] - tar[rx][y][i]     - tar[rx2][y][i]
            - tar[lx2][dy][i]   - tar[lx][dy][i]    - tar[x][dy][i]     - tar[rx][dy][i]    - tar[rx2][dy][i]
            - tar[lx2][dy2][i]  - tar[lx][dy2][i]   - tar[x][dy2][i]    - tar[rx][dy2][i]   - tar[rx2][dy2][i]
        ) /100 | 0
        if (final > 255) final = 255;
        if (final < 0) final = 0;
        return final;
    }
    let configName = '';
    configName = `test6_突出${D_941}_`; 
    p.writeFile(`./${configName}${new Date().getTime()}.png`)
    console.log(pixArr);
})