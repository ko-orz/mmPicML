const getPixels = require("get-pixels");
const fs = require("fs");

function getPixelsSync(path) {
    return new Promise((res, rej)=>{
        getPixels(path, (err, pixels)=>{
            if (err) rej(err);
            else res(pixels);
        })
    })
}

// class MmPixels {
//     constructor(imgPath) {
//         this.imgPath = imgPath;
//         this.pixels = await getPixelsSync(imgPath);
//     }

// }

async function MmPixels(imgPath) {
    this.imgPath = imgPath;
    this.pixels = await getPixelsSync(imgPath);
}

module.exports = MmPixels;