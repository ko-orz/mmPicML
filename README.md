# mmPicML

#### 项目介绍
根据自己的理解，用Javascript实现浅层神经网络模型的机器学习代码

#### 使用说明

1. 运行 ./main.js；
2. 数据在 data/demo1.json 中，可自行修改尝试；

#### 文章推荐

1. 机器学习方法概要的文章 [手把手教你在Python中实现文本分类（附代码、数据集）](https://www.cnblogs.com/pythonedu/p/9101778.html)